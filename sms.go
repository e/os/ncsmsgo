package ncsmsgo

type Sms struct {
	ID int `json:"_id"`
	NCID int `json:"nc_id"`
	Mailbox int `json:"mbox"`
	Type int `json:"type"`
	Date int64 `json:"date"`
	Body string `json:"body"`
	Address string `json:"address"`
	CardNumber string `json:"card_number"`
	CardSlot int `json:"card_slot"`
	IccID string `json:"icc_id"`
	DeviceName string `json:"device_name"`
	CarrierName string `json:"carrier_name"`
	Sent int `json:"sent"`
	Read string `json:"read"`
	Seen string `json:"seen"`
}

type SmsPushQuery struct {
	SmsCount int  `json:"smsCount"`
	SmsData []Sms `json:"smsDatas"`
}