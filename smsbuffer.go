package ncsmsgo

import (
	"encoding/json"
)

type SmsBuffer struct {
	lastMessageDate int64
	spq SmsPushQuery
}

func (b *SmsBuffer) Clear() {
	b.lastMessageDate = 0
	b.spq = SmsPushQuery{}
}

func (b *SmsBuffer) Empty() bool {
	return len(b.spq.SmsData) == 0
}

func (b *SmsBuffer) AsRawJSONString() string {
	bspq, err := json.Marshal(b.spq)
	if err != nil {
		println("Failed to generate JSON String from SmsPushQuery")
		return ""
	}
	return string(bspq)
}

func (b *SmsBuffer) Push(ID int, NCID int, mailboxID int, t int, date int64, address string, card_number string, card_slot int, icc_id string, device_name string, carrier_name string, body string, sent int, read string,
	seen string) {
	b.spq.SmsData = append(b.spq.SmsData, Sms{
		ID: ID,
		NCID: NCID,
		Mailbox: mailboxID,
		Type: t,
		Date: date,
		Address: address,
		CardNumber: card_number,
		CardSlot: card_slot,
		IccID: icc_id,
		DeviceName: device_name,
		CarrierName: carrier_name,
		Body: body,
		Sent: sent,
		Read: read,
		Seen: seen,
	})

	b.spq.SmsCount++

	if date > b.lastMessageDate {
		b.lastMessageDate = date
	}
}

func (b *SmsBuffer) GetLastMessageDate() int64 {
	return b.lastMessageDate
}
